import datetime
import sys

class Activity(object):
	def __init__(self,user_id,duration,rotations,calories,date=datetime.date(2015,3,1)):
		self.user_id=user_id
		self.duration=duration
		self.rotations=rotations
		self.calories=calories
		self.date=date

		if type(self.date)!=datetime.date:
			print 'Invalid date. Specify proper date'
			sys.exit(1)

class User(object):
	def __init__(self,id,name,email):
		self.id=id
		self.name=name
		self.email=email
	def __str__(self):	
		return self.email


def dayly_aggregation(acts):
	agg={}
	for act in acts:
		if act.user_id not in agg.keys():
				agg[act.user_id]={act.date:{'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}}
		else:
			if act.date not in agg[act.user_id].keys():
				agg[act.user_id][act.date]={'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}
			else:
				agg[act.user_id][act.date]["Duration"]+=act.duration
				agg[act.user_id][act.date]["Rotations"]+=act.rotations
				agg[act.user_id][act.date]["Calories"]+=act.calories
	print agg

	return agg



def weekly_aggregation(acts):
	agg={}
	for act in acts:
		week=act.date.strftime("%Y%W")
		if act.user_id not in agg.keys():
				agg[act.user_id]={week:{'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}}
		else:
			if week not in agg[act.user_id].keys():
				agg[act.user_id][week]={'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}
			else:
				agg[act.user_id][week]["Duration"]+=act.duration
				agg[act.user_id][week]["Rotations"]+=act.rotations
				agg[act.user_id][week]["Calories"]+=act.calories

	print agg

	return agg



def monthly_aggregation(acts):
	agg={}
	for act in acts:
		month=act.date.strftime("%Y%m")
		if act.user_id not in agg.keys():
				agg[act.user_id]={month:{'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}}
		else:
			if month not in agg[act.user_id].keys():
				agg[act.user_id][month]={'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}
			else:
				agg[act.user_id][month]["Duration"]+=act.duration
				agg[act.user_id][month]["Rotations"]+=act.rotations
				agg[act.user_id][month]["Calories"]+=act.calories
	print agg

	return agg



def yearly_aggregation(acts):
	agg={}
	for act in acts:
		year=act.date.strftime("%Y")
		if act.user_id not in agg.keys():
				agg[act.user_id]={year:{'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}}
		else:
			if year not in agg[act.user_id].keys():
				agg[act.user_id][year]={'Duration':act.duration,'Rotations':act.rotations,'Calories':act.calories}
			else:
				agg[act.user_id][year]["Duration"]+=act.duration
				agg[act.user_id][year]["Rotations"]+=act.rotations
				agg[act.user_id][year]["Calories"]+=act.calories
	print agg

	return agg
