import datetime
from Activity import Activity,User,dayly_aggregation,weekly_aggregation,monthly_aggregation,yearly_aggregation

user1=User(1,'Himakshiba','mz.himakshi@gmail.com')
user2=User(2,'Y23','y23y26031992@gmail.com')


'dayly aggregation:
'-------------------
'user:1 Activity
'===============
act1_u1=Activity(1,100,25,10)
act2_u1=Activity(1,150,75,20)

'user:2 Activity
'===============
act1_u2=Activity(2,200,100,10)
act2_u2=Activity(2,400,200,20)


'activity for default date:2015/3/1
add=dayly_aggregation([act1_u1,act2_u1,act1_u2,act2_u2])
add

{1: {datetime.date(2015, 3, 1): {'Duration': 250, 'Calories': 30, 'Rotations': 100}}, 
 2: {datetime.date(2015, 3, 1): {'Duration': 600, 'Calories': 30, 'Rotations': 300}}
}


'weekly aggregation:
'-------------------
'user:1 Activity
'===============

'8th week
act3_u1=Activity(1,200,55,30,datetime.date(2015,3,1))

'9th week
act4_u1=Activity(1,250,45,40,datetime.date(2015,3,2))
act5_u1=Activity(1,300,100,100,datetime.date(2015,3,7))

'10th week
act6_u1=Activity(1,200,55,30,datetime.date(2015,3,8))
act7_u1=Activity(1,250,45,40,datetime.date(2015,3,10))
act8_u1=Activity(1,300,100,100,datetime.date(2015,3,14))

'user:2 Activity
'===============
'9th week
act3_u2=Activity(2,600,300,30,datetime.date(2015,3,3))
act4_u2=Activity(2,800,400,40,datetime.date(2015,3,4))
act5_u2=Activity(2,1000,500,50,datetime.date(2015,3,6))
'10th week
act6_u2=Activity(2,1200,600,60,datetime.date(2015,3,11))
act7_u2=Activity(2,1400,700,70,datetime.date(2015,3,9))
act8_u2=Activity(2,1600,800,80,datetime.date(2015,3,12))


add=weekly_aggregation([act3_u1,act4_u1,act5_u1,act6_u1,act7_u1,act8_u1,act3_u2,act4_u2,act5_u2,act6_u2,act7_u2,act8_u2])
add

{1: {'201510': {'Duration': 550, 'Calories': 140, 'Rotations': 145}, 
     '201508': {'Duration': 200, 'Calories': 30, 'Rotations': 55}, 
     '201509': {'Duration': 750, 'Calories': 170, 'Rotations': 200}}, 
 2: {'201510': {'Duration': 4200, 'Calories': 210, 'Rotations': 2100}, 
     '201509': {'Duration': 2400, 'Calories': 120, 'Rotations': 1200}}
}


'monthly aggregation:
'--------------------
'user:1 Activity
'===============
act9_u1=Activity(1,600,300,30,datetime.date(2015,1,3))
act10_u1=Activity(1,800,400,40,datetime.date(2015,2,4))
act11_u1=Activity(1,1000,500,50,datetime.date(2015,3,6))
act12_u1=Activity(1,1200,600,60,datetime.date(2015,1,11))
act13_u1=Activity(1,1400,700,70,datetime.date(2015,2,9))
act14_u1=Activity(1,1600,800,80,datetime.date(2015,3,12))

'user:2 Activity
'===============
act9_u2=Activity(2,1200,600,60,datetime.date(2015,4,1))
act10_u2=Activity(2,1400,700,70,datetime.date(2015,4,2))
act11_u2=Activity(2,1600,800,80,datetime.date(2015,5,4))
act12_u2=Activity(2,1800,900,90,datetime.date(2015,5,5))

add=monthly_aggregation([act9_u1,act10_u1,act11_u1,act12_u1,act13_u1,act14_u1,act9_u2,act10_u2,act11_u2,act12_u2])
add

{1: {'201502': {'Duration': 2200, 'Calories': 110, 'Rotations': 1100}, 
     '201503': {'Duration': 2600, 'Calories': 130, 'Rotations': 1300}, 
     '201501': {'Duration': 1800, 'Calories': 90, 'Rotations': 900}}, 
 2: {'201504': {'Duration': 2600, 'Calories': 130, 'Rotations': 1300}, 
     '201505': {'Duration': 3400, 'Calories': 170, 'Rotations': 1700}}
}



'yearly aggregation:
'-------------------
'user:1 Activity
'===============
act15_u1=Activity(1,600,300,30,datetime.date(2013,1,3))
act16_u1=Activity(1,800,400,40,datetime.date(2013,2,4))
act17_u1=Activity(1,1000,500,50,datetime.date(2014,3,6))
act18_u1=Activity(1,1200,600,60,datetime.date(2014,1,11))
act19_u1=Activity(1,1400,700,70,datetime.date(2015,2,9))
act20_u1=Activity(1,1600,800,80,datetime.date(2015,3,12))

'user:2 Activity
'===============
act13_u2=Activity(2,1200,600,60,datetime.date(2014,4,1))
act14_u2=Activity(2,1400,700,70,datetime.date(2014,4,2))
act15_u2=Activity(2,1600,800,80,datetime.date(2015,5,4))
act16_u2=Activity(2,1800,900,90,datetime.date(2015,5,5))
act17_u2=Activity(2,2000,800,80,datetime.date(2015,5,4))

add=yearly_aggregation([act15_u1,act16_u1,act17_u1,act18_u1,act19_u1,act20_u1,act13_u2,act14_u2,act15_u2,act16_u2,act17_u2])
add

{1: {'2015': {'Duration': 3000, 'Calories': 150, 'Rotations': 1500}, 
     '2014': {'Duration': 2200, 'Calories': 110, 'Rotations': 1100}, 
     '2013': {'Duration': 1400, 'Calories': 70, 'Rotations': 700}}, 
 2: {'2015': {'Duration': 5400, 'Calories': 250, 'Rotations': 2500}, 
     '2014': {'Duration': 2600, 'Calories': 130, 'Rotations': 1300}}
}
